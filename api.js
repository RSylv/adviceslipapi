let url = 'https://api.adviceslip.com/advice';

let affiche = document.getElementById('papers');
let traduction = document.getElementById('translate');
let afficheError = document.getElementById('error');

function refresh() {
    affiche.style.display = 'none';
    traduction.style.display = 'none';
    afficheError.style.display = 'none';
}
refresh();

function search() {
    refresh();

    let search = document.getElementById('search').value;
    let newUrl = url + '/search/' + search;

    fetch(newUrl)
        .then(response => {
            return response.json();
        })
        .then(data => {

            for (const key in data['slips']) {
                totalRes = data['total_results'];
                document.getElementById('totalSearch').innerHTML = totalRes;
                // if new click on search
                randkey = (Math.floor(Math.random(key) * totalRes));

                affiche.style.display = 'block';
                affiche.innerHTML = data['slips'][randkey]['advice'];
                traduction.style.display = 'block';
            }
        })
        .catch(error)
}
error = function () {
    afficheError.style.display = 'block';
    afficheError.innerHTML = "Aucun conseil trouvé.. essaye a nouveau !";
}

function random() {
    refresh();
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            affiche.style.display = 'block';
            affiche.innerHTML = data["slip"]["advice"];
            traduction.style.display = 'block';
        })
}



